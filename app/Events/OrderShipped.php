<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderShipped
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }


    public function broadcastOn()
    {
        dd('event broadcastOn : ' . $this->user);
        return new PrivateChannel('channel-name');
    }
}
