<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{

    public function login(Request $request)
    {
        $fields = $request->validate([
            'email'=>'required',
            'password' =>'required'
        ]);

        //check email
        $user = User::where('email', $fields['email'])->first();

        //check password
        if(!$user || !Hash::check($fields['password'],$user->password)){
            return response(['status'=>false,'message'=>'invalid email or password'],401);
        }

        //create token
        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'status'=>true,
            'message'=>'Login successful!',
            'data' =>[
                'user'=> $user,
                'token'=> $token
            ]
        ];
        return response($response,201);
    }

    public function logout(Request $request)
    {

        dd(auth()->user()->currentAccessToken());
        auth()->user()->currentAccessToken()->delete();

        $response = [
            'status'=>true,
            'message'=>'Logout successfully',
        ];  
        return response($response,201);
    }

}
