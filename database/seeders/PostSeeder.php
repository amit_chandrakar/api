<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Laravel 9 is Now Released!',
                'status' => 'active',
                'description' => 'Laravel 9 is now released and includes many new features, including a minimum PHP v8.0 version, controller route groups, a refreshed default Ignition error page, Laravel Scout database engine, Symfony mailer integration, Flysystem 3.x, Improved Eloquent accessors/mutators, and many more features.

                Before we jump into the new features, we\'d like to point out that starting with Laravel 9, Laravel will release a new major version about every twelve months instead of the previous six-month schedule:

                Laravel uses a variety of community-driven packages as well as nine Symfony components for a number of features within the framework. Symfony 6.0 is due for release in November. For that reason, we are choosing to delay the Laravel 9.0 release until 2022.

                By delaying the release, we can upgrade our underlying Symfony components to Symfony 6.0 without being forced to wait until September 2022 to perform this upgrade. In addition, this better positions us for future releases as our yearly releases will always take place two months after Symfony’s releases.',
            ],
            [
                'name' => 'Laracon is this week!',
                'status' => 'active',
                'description' => 'Join us this Wednesday, February 9th, 2022, for this year\'s Laracon Online winter edition. For the first time, we will be streaming Laracon for free on YouTube, allowing us to reach the entire Laravel community.

                The live stream will start around 8:55 AM for the opening remarks, and then it\'ll go directly into the first talk. You can check out the full schedule on the Laracon website or see it below. Also, Taylor will be giving an update on Laravel 9.',
            ],
            [
                'name' => 'PHP Monitor 5.0 for macOS is Here',
                'status' => 'active',
                'description' => 'PHP Monitor, the lightweight native Mac app to manage PHP and Laravel Valet, has released v5.0!

                The author Nico Verbruggen announced the release yesterday:',
            ],
            [
                'name' => 'Using S3 with Laravel',
                'status' => 'active',
                'description' => 'AWS S3 provides a place for us to store files off of our servers. There are some big benefits to this:

                Backup/redundancy - S3 and similar have built-in backups and redundancy
                Scaling - Savings files off-server becomes necessary in modern hosting, such as serverless or containerized environments, as well as in traditional load-balanced environments
                Disk usage - You won\'t need as much disk space when storing files in the cloud
                Features - S3 (and other clouds) have some great features, such as versioning support for files, lifecycle rules for deleting old files (or storing them in a cheaper way), deletion protection, and more
                Using S3 now (even in single-server setups) can reduce headaches in the long run. Here\'s what you should know!',
            ],
        ];

        Post::insert($data);
    }
}
