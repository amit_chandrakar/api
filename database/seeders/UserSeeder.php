<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@example.com';
        $user->password = Hash::make('123456');
        $user->save();
        
        $user = new User();
        $user->name = 'Employee';
        $user->email = 'employee@example.com';
        $user->password = Hash::make('123456');
        $user->save();
    }
}
